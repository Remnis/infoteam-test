﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeverityService.Models {
    public class Severityes {
        [Key]
        public int SystemSerialNumber { get; set; }
        public int SystemMaterialNumber { get; set; }
        public string SystemName { get; set; }
        public int TubeSerialNumber { get; set; }
        public int TubeMaterialNumber { get; set; }
        public string TubeName { get; set; }
    }
}
