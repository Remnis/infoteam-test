﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeverityService.Models {
    public class DbRepository : ISeverityRepository {
        private readonly SeverityContext _db;

        public DbRepository( SeverityContext db ) {
            this._db = db;
        }

        public IQueryable<SeverityItem> ServerityItems => _db.ServerityItems;

        public IQueryable<Severityes> Serverityes => _db.Severityes;

        public void Add<EntityType>( EntityType entity ) {
            _db.Add(entity);
        }

        public void SaveChanges() {
            _db.SaveChanges();
        }
    }
}
