﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeverityService.Models {
    public interface ISeverityRepository {
        public IQueryable<SeverityItem> ServerityItems { get; }

        public IQueryable<Severityes> Serverityes { get; }
        void Add<EntityType>( EntityType entity );

        void SaveChanges();
    }
}
