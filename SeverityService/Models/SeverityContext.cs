﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace SeverityService.Models {
    public class SeverityContext : DbContext {
        public SeverityContext(DbContextOptions<SeverityContext> options) : base(options) {

        }
        public DbSet<SeverityItem> ServerityItems {
            get;
            set;
        }

        public DbSet<Severityes> Severityes {
            get;
            set;
        }
    }
}
