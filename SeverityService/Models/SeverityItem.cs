﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeverityService.Models
{
	public class SeverityItem
	{
		[Key]
		public int SystemSerialNumber { get; set; }
		public string Plane { get; set; }
		public int TubeSerialNumber { get; set; }
		public DateTime DateTimeThMax { get; set; }
		public int GeneralSeverity { get; set; }



	}
}
