﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeverityService.Models;

namespace SeverityService.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class SeverityController : ControllerBase {
        private readonly ISeverityRepository _context;

        public SeverityController(ISeverityRepository context) {
            _context = context;
        }

        // GET: /api​/severity
        [HttpGet]
        public IEnumerable<SeverityItem> GetServerityItems() {
            return _context.ServerityItems.ToList();
        }

        [Route("/api/severityasync")]
        // GET: /api​/severityasync
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SeverityItem>>> GetServerityItemsAsync() {
            return await _context.ServerityItems.ToListAsync();
        }

        // GET: ​/api​/severity​/severityes/{systemSerialNumber}
        [Route("severityes/{systemSerialNumber}")]
        [HttpGet("{systemSerialNumber}")]
        public ActionResult<IEnumerable<Severityes>> GetServerityItem(string systemSerialNumber) {

            if (int.TryParse(systemSerialNumber, out int id)) {
                var serverityItem = _context.Serverityes.Where(p => p.SystemSerialNumber == id);
                if (serverityItem != null) {
                    return serverityItem.ToList();
                }
            }
           
            return NotFound();

        }







    }
}
