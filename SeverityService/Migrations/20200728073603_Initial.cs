﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeverityService.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServerityItems",
                columns: table => new
                {
                    SystemSerialNumber = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SystemMaterialNumber = table.Column<int>(nullable: false),
                    Plane = table.Column<string>(nullable: true),
                    TubeSerialNumber = table.Column<int>(nullable: false),
                    DateTimeThMax = table.Column<DateTime>(nullable: false),
                    GeneralSeverity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServerityItems", x => x.SystemSerialNumber);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServerityItems");
        }
    }
}
