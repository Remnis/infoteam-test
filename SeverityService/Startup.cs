using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using SeverityService.Models;

namespace SeverityService {
    public class Startup {
        public Startup( IConfiguration configuration ) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services ) {
            if(Configuration["AppSettings:useInMemoryDb"] == "0") {
                // ms-sql
                services.AddDbContext<SeverityContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("mssqldb")));
            }
            else {
                // InMemory
                services.AddDbContext<SeverityContext>(opt => opt.UseInMemoryDatabase("SeverityList"));
                 services.AddMvc();

            }
            services.AddTransient<ISeverityRepository, DbRepository>();
            services.AddScoped<ISeverityRepository, DbRepository>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            if(env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                // filling wiith test-data
                var context = app.ApplicationServices.CreateScope().ServiceProvider.GetService<SeverityContext>();
                for(int i=0; i < 10; i++) {
                    AddTestData(context, i);
                }
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }

        private static void AddTestData(SeverityContext context, int serial) {
            Random r = new Random();
            var test = new SeverityItem { DateTimeThMax = DateTime.Now, GeneralSeverity = 2, Plane = "Plane", SystemSerialNumber = r.Next(999,5555555) , TubeSerialNumber = r.Next(10000, 50000) };
            context.ServerityItems.Add(test);
            var test2 = new Severityes { SystemMaterialNumber = r.Next(100, 500), SystemSerialNumber = (5000 + serial), SystemName = "Tube", TubeMaterialNumber = r.Next(7777, 8888), TubeName = "Tube-Siemens", TubeSerialNumber = r.Next(8888, 99999) };
            context.Severityes.Add(test2);
            context.SaveChanges();
        }

    }


}
