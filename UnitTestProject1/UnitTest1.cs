using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SeverityService.Controllers;
using SeverityService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {

        private SeverityController _controller;
        private ISeverityRepository _repository;

        private void Populate(SeverityContext context)
        {
            for(int i = 0; i < 10; i++)
            {
                var item = new SeverityItem { DateTimeThMax = DateTime.Now, SystemSerialNumber = i, GeneralSeverity = (i * 2), Plane = "Plane", SystemMaterialNumber = (i * 3), TubeSerialNumber = (i * 4) };
                context.Add(item);
                context.SaveChanges();
            }
            
        }

        private ISeverityRepository GetInMemoryRepository()
        {
            var options = new DbContextOptionsBuilder<SeverityContext>()
                             .UseInMemoryDatabase(databaseName: "MemoryDb")
                             .Options;
            var initContext = new SeverityContext(options);

            initContext.Database.EnsureDeleted();

            Populate(initContext);

            var testContext = new SeverityContext(options);

            var repository = new DbRepository(testContext);

            return repository;
        }

        [TestInitialize]
        public void Setup()
        {
            _repository = GetInMemoryRepository();
            _controller = new SeverityController(_repository);
        }

        [TestMethod]
        public void ListItems()
        {
            var result = _controller.GetServerityItems() as ViewResult;
            var model = result.Model as IEnumerable<SeverityItem>;

            Assert.AreEqual(10, model.Count());
        }

       
    }
}
